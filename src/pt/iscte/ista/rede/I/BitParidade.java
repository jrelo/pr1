package pt.iscte.ista.rede.I;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by jrel on 2016-04-05.
 */
public class BitParidade extends Testes {

    private static boolean PARITY = true;

    public BitParidade() {
        super(12);
    }

    @Override
    public Testes setDataTrama(int dataTrama) {
        super.dataTrama = dataTrama;
        sendedTrama = dataTrama * 2;
        sendedTrama += Integer.bitCount(dataTrama) % 2 == 1 ? 1 : 0;
        recivedTrama = Trama.getResponce(sendedTrama, 0);
        return this;
    }


    @Override
    public Testes setErrorTrama(int errorTrama) {
        this.errorTrama = errorTrama;
        this.recivedTrama = Trama.getResponce(sendedTrama, errorTrama);
        return this;
    }


    @Override
    public boolean erro() {
        return Integer.bitCount(recivedTrama) % 2 == 1;
    }


    @Override
    String getNome() {
        return "Bit Paridade";
    }

    @Override
    int correcao() {
        return -1;
    }


    public static void main(String[] args) {
        System.out.println(new BitParidade().setDataTrama(531).setErrorTrama(0).erro());
    }
}


