package pt.iscte.ista.rede.I;

/**
 * Created by jrel on 2016-04-05.
 */
public abstract class Testes {
    protected int sendedTramaSize = 0;
    static protected int TRAMA_DATA_SIZE = 11;

    protected int dataTrama;
    protected int sendedTrama;
    protected int errorTrama;
    protected int recivedTrama;


    public Testes(int sendedTramaSize) {
        this.sendedTramaSize = sendedTramaSize;
    }

    abstract Testes setDataTrama(int dataTrama);

    abstract Testes setErrorTrama(int errorTrama);

    abstract boolean erro();

    public int getDataTrama() {
        return dataTrama;
    }

    public String getDataTramaString() {
        return Trama.toString(dataTrama, TRAMA_DATA_SIZE);
    }

    public int getSendedTrama() {
        return sendedTrama;
    }

    public String getSendedTramaString() {
        return Trama.toString(sendedTrama, sendedTramaSize);
    }

    public int getErrorTrama() {
        return errorTrama;
    }

    public String getErrorTramaString() {
        return Trama.toString(errorTrama, sendedTramaSize);
    }

    public int getRecivedTrama() {
        return this.recivedTrama;
    }

    public String getRecivedTramaString() {
        return Trama.toString(recivedTrama, sendedTramaSize);
    }

    public int getTRAMA_SENDED_SIZE() {
        return sendedTramaSize;
    }

    abstract String getNome();

    public String erroString() {
        return erro() ? "Houve erro" : "Não houve erro";
    }

    abstract int correcao();


}
