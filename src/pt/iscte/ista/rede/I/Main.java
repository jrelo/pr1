package pt.iscte.ista.rede.I;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Testes teste = null;
        System.out.print("Escolha da técnica (1:Bit paridade, 2:Hamming, 3:CRC(17): ");
        String tecnica = in.nextLine();

        if (tecnica.equals("1")) teste = new BitParidade();
        else if (tecnica.equals("2")) teste = new Hamming();
        else if (tecnica.equals("3")) teste = new CRC();

        System.out.print("Bits de dados?     D: ");
        teste.setDataTrama(Integer.parseInt(in.nextLine(), 2));
        System.out.println("> Trama enviada    T: " + teste.getSendedTramaString());
        System.out.print("Padrão de erros?   E: ");
        teste.setErrorTrama(Integer.parseInt(in.nextLine(), 2));
        System.out.println("> Trama recebida,  R: " + teste.getRecivedTramaString());
        System.out.println("> Detecção de erro:" + teste.erroString());
    }
}
