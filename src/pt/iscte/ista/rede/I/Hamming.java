package pt.iscte.ista.rede.I;

/**
 * Created by jrel on 2016-04-05.
 */
public class Hamming extends Testes {


    private static int HAMMING_1 = 0b11011010101;
    private static int HAMMING_2 = 0b10110110011;
    private static int HAMMING_3 = 0b01110001111;
    private static int HAMMING_4 = 0b00001111111;
    private static int HAMMING_1_REVERT = 0b101010101010101;
    private static int HAMMING_2_REVERT = 0b011001100110011;
    private static int HAMMING_3_REVERT = 0b000111100001111;
    private static int HAMMING_4_REVERT = 0b000000011111111;
    private int correcao;

    public Hamming() {
        super(TRAMA_DATA_SIZE + 4);
    }

    @Override
    public Testes setDataTrama(int dataTrama) {
        this.dataTrama = dataTrama;


        int value = 0;
        value = (value + (Trama.bitParity(dataTrama & HAMMING_1, true) ? 1 : 0)) * 2;
        value = (value + (Trama.bitParity(dataTrama & HAMMING_2, true) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 10) ? 1 : 0)) * 2;
        value = (value + (Trama.bitParity(dataTrama & HAMMING_3, true) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 9) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 8) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 7) ? 1 : 0)) * 2;
        value = (value + (Trama.bitParity(dataTrama & HAMMING_4, true) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 6) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 5) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 4) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 3) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 2) ? 1 : 0)) * 2;
        value = (value + (Trama.getByte(dataTrama, 1) ? 1 : 0)) * 2;
        value += (Trama.getByte(dataTrama, 0) ? 1 : 0);


        this.sendedTrama = value;


        return this;
    }


    @Override
    public Testes setErrorTrama(int errorTrama) {
        this.errorTrama = errorTrama;
        this.recivedTrama = Trama.getResponce(sendedTrama, errorTrama);
        erro();
        return this;
    }


    @Override
    public boolean erro() {
        int erro = Trama.bitParity(recivedTrama & HAMMING_1_REVERT, true) ? 1 : 0;
        erro += Trama.bitParity(recivedTrama & HAMMING_2_REVERT, true) ? 2 : 0;
        erro += Trama.bitParity(recivedTrama & HAMMING_3_REVERT, true) ? 4 : 0;
        erro += Trama.bitParity(recivedTrama & HAMMING_4_REVERT, true) ? 8 : 0;
        if (erro != 0)
            correcao = recivedTrama ^ (int) Math.pow(2, sendedTramaSize - erro);
        else
            correcao = recivedTrama;
        return erro != 0;
    }


    @Override
    String getNome() {
        return "Ham(15,11)";
    }

    @Override
    int correcao() {
        return correcao;
    }

    @Override
    public String erroString() {
        return super.erroString() + ". Correção " + Trama.toString(correcao, sendedTramaSize);
    }
}
