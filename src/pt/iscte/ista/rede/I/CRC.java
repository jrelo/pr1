package pt.iscte.ista.rede.I;

/**
 * Created by jrel on 2016-04-05.
 */

class CRC extends Testes {


    CRC() {
        super(TRAMA_DATA_SIZE + 4);
    }

    @Override
    public Testes setDataTrama(int dataTrama) {
        this.dataTrama = dataTrama;

        this.sendedTrama = (int) (dataTrama * Math.pow(2, sendedTramaSize - TRAMA_DATA_SIZE));
        this.sendedTrama += getRest(sendedTrama);
        return this;
    }

    @Override
    public Testes setErrorTrama(int errorTrama) {
        this.errorTrama = errorTrama;
        this.recivedTrama = Trama.getResponce(sendedTrama, errorTrama);
        return this;
    }

    @Override
    public boolean erro() {
        return getRest(recivedTrama) != 0;
    }

    private int getRest(int value) {

        boolean c0 = false,
                c1 = false,
                c2 = false,
                c3 = false,
                c4 = false,
                c0_novo = false,
                c1_novo = false,
                c2_novo = false,
                c3_novo = false,
                c4_novo = false;

        for (int i = 0; i < sendedTramaSize; i++) {
            c0_novo = c4 ^ Trama.getByte(value, sendedTramaSize - i - 1);
            c1_novo = c0;
            c2_novo = c1;
            c3_novo = c2;
            c4_novo = c3;

            c0 = c0_novo;
            c1 = c1_novo;
            c2 = c2_novo;
            c3 = c3_novo;
            c4 = c4_novo;
        }

        int toReturn = 0b0000;
        toReturn += c3 ? 0b1000 : 0;
        toReturn += c2 ? 0b0100 : 0;
        toReturn += c1 ? 0b0010 : 0;
        toReturn += c0 ? 0b0001 : 0;
        return toReturn;
    }

    @Override
    String getNome() {
        return "CRC17";
    }

    @Override
    int correcao() {
        return -1;
    }
}
