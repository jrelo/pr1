package pt.iscte.ista.rede.I;


import java.util.Arrays;

public class Trama {


    static boolean getByte(int trama, int place) {
        return (trama & (int) Math.pow(2.0, (double) place)) != 0;
    }


    static int getResponce(int trama, int error) {
        return trama ^ error;
    }

    static String toString(int trama, int casas) {
        String str = Integer.toBinaryString(trama);
        char[] chars = new char[casas];
        Arrays.fill(chars, '0');
        return (String.valueOf(chars) + str).substring(str.length());
    }

    static boolean bitParity(int trama, boolean parity) {
        return Integer.bitCount(trama) % 2 == 1 ^ !parity;
    }

    public static void main(String[] args) {
        System.out.println((getByte(2, 1)));
        System.out.println(getResponce(4, 2));
        System.out.println(getResponce(4, 4));
        System.out.println(getResponce(6, 2));
        System.out.println(toString(9, 11));

        System.out.println(bitParity(9, true));
        System.out.println(bitParity(11, false));
        System.out.println(bitParity(11, true));

    }


}
