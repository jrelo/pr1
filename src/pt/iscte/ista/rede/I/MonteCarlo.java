package pt.iscte.ista.rede.I;


import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by jrel on 2016-04-08.
 */
public class MonteCarlo {
    static Random r = new Random();
    static int nTramasToral = 10000000;
    static FileWriter out;
    static FileWriter out2;

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, IOException {
        out = new FileWriter(new File("output1-" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + ".csv"));
        out2 = new FileWriter(new File("output2-" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + ".csv"));

        //PrintStream out = System.out;
        //PrintStream out2 = System.err;

        for (Class<Testes> c : new Class[]{BitParidade.class, Hamming.class, CRC.class}) {
            //for (Class<Testes> c : new Class[]{Hamming.class}) {


            Testes discart = c.newInstance();

            int size = discart.getTRAMA_SENDED_SIZE();
            out2.append("#Tecnica " + discart.getNome() + System.lineSeparator());
            out2.append("#Peb;Pse;#erros;Pnd_e;Pcc_e" + System.lineSeparator());
            for (double pErroBit : new double[]{0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 0.125, 0.25, 0.5}) {


                int[] counterIBitsErrados = new int[size + 1];
                int[] counterIErrosNaoDettados = new int[size + 1];
                int counterErrosCorrigidos = 0;
                int nErros = 0;
                int sumBitsErrados = 0;
                double probNaoDetetarError = 0;
                double probSemErros = 0;
                double probMediaErrosBit = 0;
                double probCorigirErrors = 0;

                for (int i = 0; i <= nTramasToral; i++) {
                    int trama = r.nextInt(0b100000000000);
                    int erroTrama = getTramaBasedOnProb(pErroBit, size);
                    Testes t = c.newInstance().setDataTrama(trama).setErrorTrama(erroTrama);

                    counterIBitsErrados[Integer.bitCount(erroTrama)]++;


                    if (erroTrama != 0) {
                        if (!t.erro()) {
                            counterIErrosNaoDettados[Integer.bitCount(erroTrama)]++;
                        } else if (t.correcao() == t.getSendedTrama()) {

                            counterErrosCorrigidos++;
                        }
                        nErros++;
                    }
                }

                for (int i = 0; i <= size; i++) {
                    probNaoDetetarError += counterIErrosNaoDettados[i];
                    sumBitsErrados += counterIBitsErrados[i] * i;
                }
                probNaoDetetarError = ((((double) probNaoDetetarError) / ((double) nTramasToral)));
                probSemErros = ((((double) counterIBitsErrados[0] / (double) nTramasToral)));
                probMediaErrosBit = ((((double) sumBitsErrados) / ((double) nTramasToral)));
                probCorigirErrors = ((((double) counterErrosCorrigidos) / ((double) nErros)));

                out.append("#Tecnica " + discart.getNome() + ", Peb " + pErroBit + ", " + nTramasToral + " tramas" + System.lineSeparator());
                out.append("#ne;Peb;Pi;Pnd_ei" + System.lineSeparator());

                for (int i = 0; i <= size; i++) {
                    out.append(i + ";"
                            + String.format("%f", pErroBit) + ";"
                            + String.format("%f", (double) counterIBitsErrados[i] / (double) nTramasToral) + ";"
                            + String.format("%f", (double) counterIErrosNaoDettados[i] / (double) counterIBitsErrados[i]) + System.lineSeparator());
                }

                String output = String.format("%f", pErroBit) + ";"
                        + String.format("%f", probSemErros) + ";"
                        + String.format("%f", probMediaErrosBit) + ";"
                        + String.format("%f", probNaoDetetarError) + ";"
                        + String.format("%f", probCorigirErrors) + System.lineSeparator();

                out.append("#Peb;Pse;#erros;Pnd_e;Pcc_e" + System.lineSeparator());
                out2.append(output);
                out.append("#" + output);
                out.append(System.lineSeparator());
                out.flush();
                out2.flush();
            }
        }
        out.close();
        out2.close();

    }


    private static int getTramaBasedOnProb(double erroBit, int size) {
        int toReturn = 0;

        for (int i = 0; i < size; i++) {
            if (r.nextDouble() < erroBit)
                toReturn++;
            toReturn = toReturn << 1;
        }

        toReturn = toReturn >> 1;
        return toReturn;
    }
}
